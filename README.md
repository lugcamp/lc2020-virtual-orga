# lc2020-virtual-orga

Orga-Repo für LC2020-Virtual Camp

## Warum?

In diesem Jahr kann es aufgrund der COVID-19 Situation kein F2F-Camp geben.
Das Camp hätte eigentlich vom 21.05.-24.05.20 stattgefunden.

Daher möchten wir uns Online treffen. Da es damit auch keine zentrale Orga gibt,
versuchen wir die Orga mal dezentral in einem Git-Repo...

## Wie?

Bei Gitlab kann sich jeder entweder registrieren oder mit diversen Online-Accounts anmelden.

Lasst uns die "Issues" für Vorschläge und Diskussionen verwenden. Das Git-Repo für Dokumentationen, listen und co.

Das Projekt ist zwar öffentlich, aber um direkt beitragen zu können, bitte einmal den Betritt zum Projekt beantragen
und ggf. auf der Mailingliste einmal kund tun, mit welchem Namen Ihr Euch registriert habt. Wir fügen Euch dann hinzu.

## Streaming-Systeme

(verfügbare Streaming-Server)

* https://meet.serverwg.de (LUGFL, Frank A.)
* https://meet.lug-stormarn.de/


## "C4P"

Vorschlag für Online Streaming-Sessions mit Vorträgen und co...

* Container/Kubernetes Spielereien (Frank A.)

